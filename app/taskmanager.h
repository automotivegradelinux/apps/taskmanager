#include <QObject>
#include <QString>
#include <QSharedPointer>
#include <QStringList>
#include <QVector>
#include <QtCore>
#include <memory>
#include <messageengine.h>
#include "procinfo.h"

#ifndef TASKMANAGER_H
#define TASKMANAGER_H

class TaskManager : public QObject
{
	Q_OBJECT

public:
	explicit TaskManager(QObject* parent = nullptr);
	virtual ~TaskManager();

	Q_INVOKABLE void open(const QUrl& url);
	Q_INVOKABLE void kill(int tid);
	Q_INVOKABLE void getExtraInfo(int tid);
	QTimer *timer;
	QTimer *loadAvgTimer;

signals:
	void updateProcess(const QString& cmd_, int tid_, int euid_, double scpu_, double ucpu_, double resident_memory_, const QString& state_);
	void addProcess(const QString& cmd_, int tid_, int euid_, double scpu_, double ucpu_, double resident_memory_, const QString& state_);
	void removeProcess(int tid_);
	void showProcessInfo(const QString info_);
	void updateLoadAverage(double value_);
	void updateNetworkStats(unsigned int in_, unsigned int out_);

private slots:
	void query();
	void callService(const QString& command, QJsonValue value);
	void onConnected();
	void onMessageReceived(std::shared_ptr<Message>);
	void loadAvg();

private:
	std::shared_ptr<MessageEngine> m_loop;
	std::vector<ProcInfo> m_procinfos;
	unsigned int inOctets, outOctets;

	void ProcessResponse(std::shared_ptr<Message>);
	void ProcessResponseTasklist(QJsonArray& processes);
	void ProcessResponseExtraInfo(QJsonObject& info);
	void ProcessResponseLoadAvg(QJsonObject& loadInfo);
	void ProcessResponseNetStat(QJsonObject& netstat);
};

#endif // TASKMANAGER_H
