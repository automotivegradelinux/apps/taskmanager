TARGET = taskmanager

QT = quickcontrols2 websockets charts

CONFIG(release, debug|release) {
    QMAKE_POST_LINK = $(STRIP) --strip-unneeded $(TARGET)
}

SOURCES += \
        main.cpp \
        taskmanager.cpp \
        procinfo.cpp

HEADERS = taskmanager.h

RESOURCES += qml.qrc

include(app.pri)
