TEMPLATE = app
QMAKE_LFLAGS += "-Wl,--hash-style=gnu -Wl,--as-needed"

CONFIG += link_pkgconfig
PKGCONFIG += qtappfw-core

DESTDIR = $${OUT_PWD}/../package/root/bin
