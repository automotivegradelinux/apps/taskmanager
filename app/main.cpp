#include <QtCore/QDebug>
#include <QtCore/QCommandLineParser>
#include <QtCore/QUrlQuery>
#include <QtGui/QGuiApplication>
#include <QtQml/QQmlApplicationEngine>
#include <QtQml/QQmlContext>
#include <QtQml/qqml.h>
#include <QQuickWindow>
#include <QQmlApplicationEngine>
#include <QApplication>
#include <taskmanager.h>

#include <unistd.h>

int main(int argc, char *argv[])
{
	QString graphic_role = QString("utility");
	QString myname = QString("taskmanager");

	QApplication app(argc, argv);
	app.setDesktopFileName(myname);

	QCommandLineParser parser;
	parser.addPositionalArgument("port", app.translate("main", "port for binding"));
	parser.addPositionalArgument("secret", app.translate("main", "secret for binding"));
	parser.addHelpOption();
	parser.addVersionOption();
	parser.process(app);
	QStringList positionalArguments = parser.positionalArguments();

	qmlRegisterType<TaskManager>("TaskManager", 1, 0, "TaskManager");

	QQmlApplicationEngine engine;
	if (positionalArguments.length() != 2) {
		qDebug() << "[ERROR] No port and token specified!";
		return -1;
	}

	int port = positionalArguments.takeFirst().toInt();
	QString secret = positionalArguments.takeFirst();
	QUrl bindingAddress;
	bindingAddress.setScheme(QStringLiteral("ws"));
	bindingAddress.setHost(QStringLiteral("localhost"));
	bindingAddress.setPort(port);
	bindingAddress.setPath(QStringLiteral("/api"));
	QUrlQuery query;
	query.addQueryItem(QStringLiteral("token"), secret);
	bindingAddress.setQuery(query);
	QQmlContext *context = engine.rootContext();
	context->setContextProperty(QStringLiteral("bindingAddress"), bindingAddress);
	qDebug() << "Connect to: " << bindingAddress;

	engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
	return app.exec();
}
